const allure = codeceptjs.container.plugins('allure');

const OWNER = "allure-framework";
const REPO = "allure2";
const ISSUE_TITLE = "Some issue title here";

Feature('Pull Requests');

Scenario("Create new pull request", ({I}) => {
  allure.addLabel("layer", "web");
  allure.addLabel("owner", "eroshenkoam");
  allure.addLabel("feature", "Pull Requests");
  allure.addLabel("story", "Create new pull request");
  allure.addLabel("microservice", "Billing");

  I.openIssuesPage(OWNER, REPO);
  I.createIssueWithTitle(ISSUE_TITLE);
  I.shouldSeeIssueWithTitle(ISSUE_TITLE);
});

Scenario("Close existing pull reques", ({I}) => {
  allure.addLabel("layer", "web");
  allure.addLabel("owner", "eroshenkoam");
  allure.addLabel("feature", "Pull Requests");
  allure.addLabel("story", "Close existing pull request");
  allure.addLabel("microservice", "Repository");

  I.openIssuesPage(OWNER, REPO);
  I.createIssueWithTitle(ISSUE_TITLE);
  I.shouldSeeIssueWithTitle(ISSUE_TITLE);
});
