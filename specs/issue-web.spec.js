const allure = codeceptjs.container.plugins('allure');

const OWNER = "allure-framework";
const REPO = "allure2";
const ISSUE_TITLE = "Some issue title here";

Feature('Issues');

Scenario("Creating new issue authorized user", ({I}) => {
  allure.addLabel("layer", "web");
  allure.addLabel("owner", "eroshenkoam");
  allure.addLabel("feature", "Issues");
  allure.addLabel("story", "Create new issue");
  allure.addLabel("microservice", "Billing");

  I.openIssuesPage(OWNER, REPO);
  I.createIssueWithTitle(ISSUE_TITLE);
  I.shouldSeeIssueWithTitle(ISSUE_TITLE);
});

Scenario("Adding note to advertisement", ({I}) => {
  allure.addLabel("layer", "web");
  allure.addLabel("owner", "eroshenkoam");
  allure.addLabel("feature", "Issues");
  allure.addLabel("story", "Create new issue");
  allure.addLabel("microservice", "Repository");

  I.openIssuesPage(OWNER, REPO);
  I.createIssueWithTitle(ISSUE_TITLE);
  I.shouldSeeIssueWithTitle(ISSUE_TITLE);
});

Scenario("Closing new issue for authorized user", ({I}) => {
  allure.addLabel("layer", "web");
  allure.addLabel("owner", "eroshenkoam");
  allure.addLabel("feature", "Issues");
  allure.addLabel("story", "Close existing issue");
  allure.addLabel("microservice", "Repository");

  I.openIssuesPage(OWNER, REPO);
  I.createIssueWithTitle(ISSUE_TITLE);
  I.closeIssueWithTitle(ISSUE_TITLE);
  I.shouldSeeIssueWithTitle(ISSUE_TITLE);
});
