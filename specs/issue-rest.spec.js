const allure = codeceptjs.container.plugins('allure');

const OWNER = "allure-framework";
const REPO = "allure2";
const ISSUE_TITLE = "Some issue title here";

Feature('Issues');

Scenario("Create issue via api", ({I}) => {
  allure.addLabel("layer", "rest");
  allure.addLabel("owner", "baev");
  allure.addLabel("feature", "Issues");
  allure.addLabel("story", "Create new issue");
  allure.addLabel("microservice", "Billing");
  
  I.openIssuesPage(OWNER, REPO);
  I.createIssueWithTitle(ISSUE_TITLE);
  I.shouldSeeIssueWithTitle(ISSUE_TITLE);
});

Scenario("Close issue via api", ({I}) => {
  allure.addLabel("layer", "rest");
  allure.addLabel("owner", "baev");
  allure.addLabel("feature", "Issues");
  allure.addLabel("story", "Close existing issue"); 
  allure.addLabel("microservice", "Repository");

  I.openIssuesPage(OWNER, REPO);
  I.createIssueWithTitle(ISSUE_TITLE);
  I.shouldSeeIssueWithTitle(ISSUE_TITLE);
});
