/// <reference types='codeceptjs' />
type Dummy = import('./helpers/steps');
type TestPlan = import('./helpers/testplan');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any }
  interface Methods extends Dummy, TestPlan {}
  interface I extends WithTranslation<Methods> {}
  namespace Translation {
    interface Actions {}
  }
}
